__author__ = 'phe'
__created_on__ = '11/14/14 - 2014 - 11'

import time
import argparse
start_time = time.time()

def string_or_number(s):
    try:
        z = int(s)
        return z
    except ValueError:
        try:
            z = float(s)
            return z
        except ValueError:
            return s

import pkg_resources
from pkg_resources import DistributionNotFound, VersionConflict

# dependencies can be any iterable with strings,
# e.g. file line-by-line iterator
dependencies = [
    'Pillow==2.9.0',
    'PyPDF2==1.25',
    'Wand==0.4.0',
]

# here, if a dependency is not met, a DistributionNotFound or VersionConflict
# exception is thrown.
pkg_resources.require(dependencies)

class DefaultListAction(argparse.Action):
    CHOICES = ['file', 'folder']
    def __call__(self, parser, namespace, values, option_string=None):
        if values:
            for value in values:
                if value not in self.CHOICES:
                    message = ("invalid choice: {0!r} (choose from {1})"
                               .format(value,
                                       ', '.join([repr(action)
                                                  for action in self.CHOICES])))

                    raise argparse.ArgumentError(self, message)
            setattr(namespace, self.dest, values)

if __name__ == "__main__":
    from pheDFEngine import pheDFEngine

    '''
    Usage :
            convert all pdf from folder hierarchy
                phe.py folder --pdf [your source folder] --dir [your destination folder]
            convert single pdf file
                phe.py file --pdf [your pdf file] --dir [your destination folder]
            for destination folder is optional, if you don't mention the folder, it will
            create out folder on where you call the phe.py
    '''

    ap = argparse.ArgumentParser()
    ap.add_argument('actions', nargs='*', action=DefaultListAction,
                    default = ['file'],
                    metavar='ACTION')
    ap.add_argument('-V', '--version', action='version',
                    version="%(prog)s (pheDF version 2.3)")
    ap.add_argument("-p", "--PDF", required=True,
                    help ="Path PDF or PDF Folder")
    ap.add_argument("-d", "--DIR", required=False,
                    help ="Path where you want to save converted PDF image")
    # ap.add_argument("-w", "--WIDTH", required=False,
    #                 help ="width")
    # ap.add_argument("-t", "--HEIGHT", required=False,
    #                 help ="height")
    ap.add_argument("-l", "--LEFT", required=False,
                    help ="left")
    ap.add_argument("-b", "--BOTTOM", required=False,
                    help ="bottom")
    ap.add_argument("-c", "--COMPRESS", required=False,
                    help ="Set compression level for image")

    args = ap.parse_args()

    left, bottom, width, height, compress = (None, None, None, None, None)
    if args.LEFT is not None:
        left = string_or_number(args.LEFT)
        if type(left) is not int:
            left = None
    if args.BOTTOM is not None:
        bottom = string_or_number(args.BOTTOM)
        if type(bottom) is not int:
            bottom = None
    # if args.WIDTH is not None:
    #     width = string_or_number(args.WIDTH)
    #     if type(width) is not int:
    #         width = None
    # if args.HEIGHT is not None:
    #     height = string_or_number(args.HEIGHT)
    #     if type(height) is not int:
    #         height = None
    if args.COMPRESS is not None:
        compress = string_or_number(args.COMPRESS)
        if type(compress) is not int:
            compress = None

    matrix = left, bottom, compress #width , height, compress
    phe = pheDFEngine(args.PDF, args.DIR, matrix)
    # if (args.WIDTH in locals() or args.WIDTH in globals()):
    #     phe.width = args.WIDTH
    # if (args.HEIGHT in locals() or args.HEIGHT in globals()):
    #     phe.height = args.HEIGHT
    if (args.LEFT in locals() or args.LEFT in globals()):
        phe.leftIndent = args.LEFT
    if (args.BOTTOM in locals() or args.BOTTOM in globals()):
        phe.bottomIndent = args.BOTTOM
    if (args.actions[0] == 'file'):
        out = phe.filePDF()
    else:
        out = phe.searchPDF()

    print(out)
    print("--- done in {0} seconds ---".format((time.time() - start_time)))
else:
    pass
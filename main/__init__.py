__author__ = 'phe'
__created_on__ = '11/14/14 - 2014 - 11'

if __name__ == "__main__":
    pass
else:
    from PyPDF2 import PdfFileReader, PdfFileWriter
    from wand.image import Image
    import os
    import StringIO
    import fnmatch

    class PheDF(object):

        def searchPDF(self):
            for root, dirnames, filenames in os.walk(self.pathnye):
                for filename in fnmatch.filter(filenames, '*.pdf'):
                    dir_path = os.path.abspath(os.path.join(self.dirnye, filename[filename.rfind("/") + 1:].replace(".pdf", "")))
                    self.pathnye = dir_path
                    file_path = os.path.abspath(os.path.join(root, filename))
                    if not os.path.exists(dir_path):
                        os.makedirs(dir_path)
                    self.goConvert(file_path)

        def __init__(self, pathnya, dirnya, matrixnya):
            self.matrixnye = matrixnya
            # self.LEFT, self.BOTTOM, self.WIDTH, self.HEIGHT, self.COMPRESS = matrixnya
            self.LEFT, self.BOTTOM, self.COMPRESS = matrixnya
            self.pathnye = os.path.abspath(pathnya)
            self.dirnye = self.defaultDir() if dirnya is None else self.checkDirPath(os.path.abspath(dirnya))
            self.verfifyDirPath(self.dirnye)
            self.setLeftIndent()
            self.setButtomIndent()
            # self.setHeight()
            # self.setWidth()

        def defaultDir(self):
            return os.getcwd() + "/out/"

        def checkDirPath(self, pathdir):
            if pathdir[-1] != "/":
                return pathdir + "/"
            else:
                return pathdir

        def verfifyDirPath(self, pathdir):
            if os.path.exists(pathdir):
                return pathdir
            else:
                os.makedirs(pathdir)
                return pathdir

        def setLeftIndent(self):
            self.leftIndent = self.LEFT

        def setButtomIndent(self):
            self.bottomIndent = self.BOTTOM

        # def setHeight(self):
        #     self.height = self.HEIGHT
        #
        # def setWidth(self):
        #     self.width = self.WIDTH

        def pdf_page_to_png(self, pagenya, loop):
            # img = Image(file=pagenya)
            # img.compression = 'zip'
            # img = img.convert('png')
            # return img
            filename = self.pathnye[self.pathnye.rfind("/") + 1:].replace(".pdf", "")
            with Image() as img:
                io = StringIO.StringIO()
                img.read(file=pagenya, resolution=self.COMPRESS)
                img.compression = 'jpeg'
                # print(int(img.width), int(img.height))
                if None not in self.matrixnye:
                    if (img.width > img.height):
                        img.crop(img.width/2 + self.leftIndent, self.bottomIndent, width=(int(img.width/2) - self.leftIndent*2), height=(int(img.height) - self.bottomIndent*2))
                    else:
                        img.crop(self.leftIndent, self.bottomIndent, width=(int(img.width) - self.leftIndent*2), height=(int(img.height) - self.bottomIndent*2))
                # img.compression_quality = 84
                toJPG = img.convert('jpeg')
                toJPG.save(filename=self.dirnye+filename+"/{}-page-{}.{}".format(filename, loop, toJPG.format))
                # pimg = PIMG.open(io)
                # # ImageFile.MAXBLOCK = max(pimg.size)**3
                # pimg.save(self.dirnye+filename+"/{}-page-{}.{}".format(filename, loop, img.format))
                print(self.dirnye+filename+"/{}-page-{}.{}".format(filename, loop, toJPG.format))


        def checkPath(self):
            pass

        def goConvert(self, filenye):
            if self.COMPRESS is None:
                self.COMPRESS = 100
                temp = list(self.matrixnye)
                temp[-1] = self.COMPRESS
                self.matrixnye = tuple(temp)
            _open = PdfFileReader(file(filenye, 'rb'))
            filename = self.pathnye[self.pathnye.rfind("/") + 1:].replace(".pdf", "")

            for loop in xrange(_open.getNumPages()):
                stream = StringIO.StringIO()
                fileDF = _open.getPage(loop)
                # fileDF.scaleBy(1.5)
                write = PdfFileWriter()
                write.addPage(fileDF)
                write.write(stream)
                stream.seek(0)
                # output = open(self.dirnye+filename+"/{}-page-{}.{}".format(filename, loop, "pdf"), 'wb')
                # write.write(output)
                # print(self.dirnye+filename+"/{}-page-{}.{}".format(filename, loop, "pdf"))
                self.pdf_page_to_png(stream, loop)
            # filename = filenye[filenye.rfind("/") + 1:].replace(".pdf", "")
            # print self.dirnye+filename+"/{}-page-{}.{}".format(filename, loop, img.format)
            # with open(self.dirnye+filename+"/stream.txt", 'w') as a:
            #     a.seek(0)
            #     a.write(stream.buf)
            # if None not in self.matrixnye:
            #     img.crop(self.leftIndent, self.bottomIndent, width=self.width, height=self.height)
            # img.save(filename=self.dirnye+filename+"/{}-page.{}".format(filename, img.format))
            # return str("file writen to " + self.dirnye+"/"+filename)
            return "Ok"

        def verify_filePDF(self):
            if not self.ver_pdf():
                return False
            if os.path.isfile(self.pathnye) and os.access(self.pathnye, os.R_OK):
                return True
            return False

        def ver_pdf(self):
            if not self.pathnye.endswith('.pdf'):
                return False
            else:
                return True

        def ver_exist(self):
            dirnye = self.pathnye[self.pathnye.rfind("/") + 1:].replace(".pdf", "")
            if os.path.isdir(self.dirnye+dirnye):
                return False
            else:
                return True

        def filePDF(self):
            if self.verify_filePDF():
                if self.ver_exist():
                    os.makedirs(self.dirnye+self.pathnye[self.pathnye.rfind("/") + 1:].replace(".pdf", ""))
                    return self.goConvert(self.pathnye)
                else:
                    return self.goConvert(self.pathnye)
